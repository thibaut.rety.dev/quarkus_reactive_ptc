package com.ptc.quarkus.reactive;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;

import javax.enterprise.context.ApplicationScoped;
import java.time.Duration;

@ApplicationScoped
public class ReactiveGreetingService {

    public Uni<String> greeting(String name) {
        return Uni.createFrom()
            .item(name)
            .onItem()
            .transform(n -> String.format("hello %s", n));
    }

    public Multi<String> greetings(
        int count,
        String name
    ) {
        {
            Multi<String> result = Multi.createFrom()
                .ticks()
                .every(Duration.ofSeconds(2))
                .onItem()
                .transform(n -> String.format("hello %s - %d", name, n))
                .transform()
                .byTakingFirstItems(count);

            System.out.println("\n*******************\n* I'm not blocked *\n*******************\n");
            result.subscribe()
                .with(e -> System.out.println("Handling " + e));
            System.out.println("\n*************************\n* I'm still not blocked *\n*************************\n");

            return result;
        }
    }
}